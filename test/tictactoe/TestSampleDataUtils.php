<?php
declare(strict_types=1);

namespace TicTacToe;

use TicTacToe\Entity\NextGenerationUser;
use TicTacToe\Entity\User;

class TestSampleDataUtils
{

    public static function makeSampleUsers(): array
    {
        return ["john" => new NextGenerationUser("john", "Cork", "John Albert"),
            "jack" => new User("jack"),
            "q" => new User("q"),
            "w" => new User("w")];
    }
}