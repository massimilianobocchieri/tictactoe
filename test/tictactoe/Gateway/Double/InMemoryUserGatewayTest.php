<?php
declare(strict_types=1);

namespace TicTacToe\Gateway\Double;

use PHPUnit\Framework\TestCase;
use TicTacToe\Entity\User;
use TicTacToe\Utility\ApplicationContext;

class InMemoryUserGatewayTest extends TestCase
{
    private $gateway;
    private $aUser;

    public function setUp()
    {
        $this->gateway = new InMemoryUserGateway();
        $this->aUser = new User("ausername");
    }

    /**
     * @expectedException TicTacToe\Exception\DuplicateUserException
     */
    public function testWhenCreatingAnExistingUser_ExceptionIsThrown()
    {
        $this->gateway->save($this->aUser); //now it exists

        $this->gateway->save($this->aUser);
    }

    /**
     * @expectedException TicTacToe\Exception\UserNotFoundException
     */
    public function testWhenDeletingANonExistentUser_ExceptionIsThrown()
    {
        $this->gateway->delete($this->aUser); //gw is empty
    }
}
