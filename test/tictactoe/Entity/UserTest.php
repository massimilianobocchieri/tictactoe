<?php
declare(strict_types=1);

namespace TicTacToe\Entity;

use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{

    private $userOne;
    private $userTwo;
    private $userThree;

    public function setUp()
    {
        $this->userOne = new User("username1");
        $this->userTwo = new User("username1");
        $this->userThree = new User("username2");
    }

    public function testSameUserInstanceIsSameCozHAsSameUsernName()
    {
        $this->assertTrue($this->userOne->isSame($this->userOne));
    }

    public function testTwoUserInstancesWithTheSameUserNameAreTheSame()
    {
        $this->assertTrue($this->userOne->isSame($this->userTwo));
    }
    public function testTwoUserInstancesWithDifferentUserNameAreNotTheSame()
    {
        $this->assertFalse($this->userOne->isSame($this->userThree));
    }

    public function testWhenStringRepresentationIsCalledRelativeAnswerIsReturned()
    {
        $this->assertEquals("I am {$this->userOne->getUserName()}", (string) $this->userOne);
    }
}
