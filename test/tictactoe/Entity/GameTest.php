<?php
declare(strict_types=1);

namespace TicTacToe\Entity;

use PHPUnit\Framework\TestCase;

class GameTest extends TestCase
{
    private $gameOne;
    private $gameTwo;

    public function setUp()
    {
        $this->gameOne = new Game();
        $this->gameTwo = new Game();
    }

    public function testSameGameInstanceIsSame()
    {
        $this->assertTrue($this->gameOne->isSame($this->gameOne));
    }

    public function testTwoInstancesOfAGameAreNotTheSame()
    {
        $this->assertFalse($this->gameOne->isSame($this->gameTwo));
    }
}
