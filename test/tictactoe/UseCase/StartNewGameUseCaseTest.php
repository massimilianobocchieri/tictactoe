<?php
declare(strict_types=1);

namespace TicTacToe;

use PHPUnit\Framework\TestCase;
use TicTacToe\Engine\TicTactToeEngineImpl;
use TicTacToe\Entity\Game;
use TicTacToe\Gateway\Double\InMemoryUserGateway;
use TicTacToe\UseCase\{
    BasicPresenter, StartNewGameUseCase
};

class StartNewGameUseCaseTest extends TestCase
{
    const SAMPLE_PLAYER_ONE_USERNAME = "john";
    const SAMPLE_PLAYER_TWO_USERNAME = "jack";
    const SAMPLE_PLAYER_ONE_WITH_NONEXISTING_USER_USERNAME = "notexistingusername1";
    const SAMPLE_PLAYER_TWO_WITH_NONEXISTING_USER_USERNAME = "notexistingusername2";

    private $useCase;
    private $gameEngine;

    public function setUp(): void
    {
        $this->gameEngine = new TicTactToeEngineImpl(new Game());

        $this->useCase = new StartNewGameUseCase(new InMemoryUserGateway(TestSampleDataUtils::makeSampleUsers()),
            new BasicPresenter(),
            $this->gameEngine);
    }

    public function testCanStartNewGameUseCase()
    {
        $this->useCase->execute(["playerOne" => self::SAMPLE_PLAYER_ONE_USERNAME,
            "playerTwo" => self::SAMPLE_PLAYER_TWO_USERNAME]);

        $presenter = $this->useCase->getPresenter();
        $this->assertNotNull($presenter->getResponse());
        $this->assertNull($presenter->getError());
    }

    public function testSameUserCanStartAGameWithHimself()
    {
        $this->useCase->execute(["playerOne" => self::SAMPLE_PLAYER_ONE_USERNAME,
            "playerTwo" => self::SAMPLE_PLAYER_ONE_USERNAME,
            "gameEngine" => new TicTactToeEngineImpl(new Game())]);

        $presenter = $this->useCase->getPresenter();
        $this->assertNotNull($presenter->getResponse());
        $this->assertNull($presenter->getError());
    }

    public function testWhenPlayersHaveNoExistingUserAssociated_ErrorIsPresented()
    {
        $this->useCase->execute(["playerOne" => self::SAMPLE_PLAYER_ONE_WITH_NONEXISTING_USER_USERNAME,
            "playerTwo" => self::SAMPLE_PLAYER_TWO_WITH_NONEXISTING_USER_USERNAME,
            "gameEngine" => $this->gameEngine]);

        $presenter = $this->useCase->getPresenter();
        $this->assertNull($presenter->getResponse());
        $this->assertEquals("USERNOTFOUND", $presenter->getError());
    }

}
