<?php
declare(strict_types=1);

namespace TicTacToe\UseCase;

use PHPUnit\Framework\TestCase;
use TicTacToe\Entity\User;
use TicTacToe\Exception\DuplicateUserException;
use TicTacToe\Gateway\Double\InMemoryUserGateway;
use TicTacToe\Utility\ApplicationContext;

class ListUsersUseCaseTest extends TestCase
{
    private $gateway;
    private $gatewayMock;

    private $useCase;

    private $existingUser;
    private $nonExistentUser;

    private $presenter;

    public static function setUpBeforeClass()
    {
        ApplicationContext::initContext();
    }

    public function setUp()
    {
        $this->gateway = new InMemoryUserGateway();
        $this->presenter = new BasicPresenter();

        $this->useCase = new ListUsersUseCase($this->gateway, $this->presenter);
    }

    public function testWhenNoUsersAreStored_EmptyAnswerIsReturned()
    {
        $this->useCase->execute();

        $data = $this->useCase->getPresenter()->getResponse()["data"];
        $this->assertEmpty($data);
    }

    public function testWhenUsersAreStored_TheyGetReturned()
    {
        $this->gateway = ApplicationContext::$userGateway;
        $this->useCase = new ListUsersUseCase($this->gateway, $this->presenter);

        $this->useCase->execute();

        $data = $this->useCase->getPresenter()->getResponse()["data"];
        $this->assertEquals(count($this->gateway->findAll()), count($data));
    }

}
