<?php
declare(strict_types=1);

namespace TicTacToe\UseCase;

use Mockery;
use PHPUnit\Framework\TestCase;
use TicTacToe\Engine\TicTactToeEngine;
use TicTacToe\Entity\GamePlayer;
use TicTacToe\Entity\User;
use TicTacToe\ValueObject\CircleShape;
use TicTacToe\ValueObject\CrossShape;

class CheckGameStatusUseCaseTest extends TestCase
{
    const SAMPLE_PLAYER_ONE_USERNAME = "john";
    const SAMPLE_PLAYER_TWO_USERNAME = "jack";

    private static $circlePlayer;
    private static $crossPlayer;

    private $useCase;
    private $presenter;
    private $gameEngineMock;


    public static function setUpBeforeClass()
    {
        self::$circlePlayer = new GamePlayer(new CircleShape(), new User(self::SAMPLE_PLAYER_ONE_USERNAME));
        self::$crossPlayer = new GamePlayer(new CrossShape(), new User(self::SAMPLE_PLAYER_TWO_USERNAME));
    }

    public function setUp(): void
    {
        $this->presenter = new BasicPresenter();
    }

    public function tearDown()
    {
        Mockery::close();
    }


    public function testWhenGameIsStillProgressResponseeportsItsStatusCorrectly()
    {
        $this->gameEngineMock = \Mockery::mock(TicTactToeEngine::class)->shouldIgnoreMissing();
        $this->gameEngineMock->allows()->checkWinner()
            ->andReturns(null);

        $this->useCase = new CheckGameStatusUseCase($this->presenter, $this->gameEngineMock);

        $this->useCase->execute();

        $this->assertEquals("INPROGRESS", $this->useCase->getPresenter()->getResponse()["status"]);
        $this->assertEmpty($this->useCase->getPresenter()->getResponse()["winner"]);
        $this->assertFalse($this->useCase->getPresenter()->getResponse()["isTie"]);
    }

    public function testWhenGameIsOverWithATieResponseReportsItCorrectly()
    {
        $this->gameEngineMock = \Mockery::mock(TicTactToeEngine::class)->shouldIgnoreMissing();
        $this->gameEngineMock->allows()->checkWinner()
            ->andReturns(null);
        $this->gameEngineMock->allows()->isTie()
            ->andReturns(true);

        $this->useCase = new CheckGameStatusUseCase($this->presenter, $this->gameEngineMock);

        $this->useCase->execute();

        $this->assertEquals("FINISHED", $this->useCase->getPresenter()->getResponse()["status"]);
    }

    public function testWhenGameIsOverWithCirclePlayerWinnerResponseReportsItCorrectly()
    {
        $this->gameEngineMock = \Mockery::mock(TicTactToeEngine::class)->shouldIgnoreMissing();
        $this->gameEngineMock->allows()->checkWinner()
            ->andReturns(self::$circlePlayer);
        $this->gameEngineMock->allows()->isTie()
            ->andReturns(false);

        $this->useCase = new CheckGameStatusUseCase($this->presenter, $this->gameEngineMock);

        $this->useCase->execute();

        $this->assertEquals("FINISHED", $this->useCase->getPresenter()->getResponse()["status"]);
        $this->assertEquals(self::$circlePlayer->getUser()->getUserName(), $this->useCase->getPresenter()->getResponse()["winner"]);
        $this->assertFalse($this->useCase->getPresenter()->getResponse()["isTie"]);
    }

    public function testWhenGameIsOverWithCrossPlayerWinnerResponseReportsItCorrectly()
    {
        $this->gameEngineMock = \Mockery::mock(TicTactToeEngine::class)->shouldIgnoreMissing();
        $this->gameEngineMock->allows()->checkWinner()
            ->andReturns(self::$crossPlayer);
        $this->gameEngineMock->allows()->isTie()
            ->andReturns(false);

        $this->useCase = new CheckGameStatusUseCase($this->presenter, $this->gameEngineMock);

        $this->useCase->execute();

        $this->assertEquals("FINISHED", $this->useCase->getPresenter()->getResponse()["status"]);
        $this->assertEquals(self::$crossPlayer->getUser()->getUserName(), $this->useCase->getPresenter()->getResponse()["winner"]);
        $this->assertFalse($this->useCase->getPresenter()->getResponse()["isTie"]);
    }
}
