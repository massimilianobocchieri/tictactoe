<?php
declare(strict_types=1);

namespace TicTacToe\UseCase;

use PHPUnit\Framework\TestCase;
use TicTacToe\Utility\ApplicationContext;

class GameUseCaseFactoryTest extends TestCase
{
    private $useCaseFactory;

    public static function setUpBeforeClass()
    {
        ApplicationContext::initContext();
    }

    public function setUp()
    {
        $this->useCaseFactory = new GameUseCaseFactory();
    }

    public function testWhenChoiceOneIsPassedACreateUserUseCaseInstanceIsReturned()
    {
        $this->assertInstanceOf(CreateUserUseCase::class, $this->useCaseFactory->makeUseCase(["choice" => 1]));
    }

    public function testWhenChoiceTwoIsPassedADeleteUserUseCaseInstanceIsReturned()
    {
        $this->assertInstanceOf(DeleteUserUseCase::class, $this->useCaseFactory->makeUseCase(["choice" => 2]));
    }

    public function testWhenChoiceThreeIsPassedAListUsersUseCaseInstanceIsReturned()
    {
        $this->assertInstanceOf(ListUsersUseCase::class, $this->useCaseFactory->makeUseCase(["choice" => 3]));
    }

    public function testWhenChoiceFourIsPassedAStartNewGameUseCaseInstanceIsReturned()
    {
        $this->assertInstanceOf(StartNewGameUseCase::class, $this->useCaseFactory->makeUseCase(["choice" => 4]));
    }

    public function testWhenChoiceFiveIsPassedAPlayGameUseCaseInstanceIsReturned()
    {
        $this->assertInstanceOf(PlayGameUseCase::class, $this->useCaseFactory->makeUseCase(["choice" => 5]));
    }

    public function testWhenChoicesSixIsPassedACheckGameStatusUseCaseInstanceIsReturned()
    {
        $this->assertInstanceOf(CheckGameStatusUseCase::class, $this->useCaseFactory->makeUseCase(["choice" => 6]));
    }

    public function testWhenAnyOtherChoiceIsPassedANotImplementedUseCaseInstanceIsReturned()
    {
        $this->assertInstanceOf(NotImplementedUseCase::class, $this->useCaseFactory->makeUseCase(["choice" => 9999]));
    }

}
