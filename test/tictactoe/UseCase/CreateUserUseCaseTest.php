<?php
declare(strict_types=1);

namespace TicTacToe\UseCase;

use Mockery;
use PHPUnit\Framework\TestCase;
use TicTacToe\Entity\User;
use TicTacToe\Exception\DuplicateUserException;
use TicTacToe\Gateway\UserGateway;
use TicTacToe\Utility\ApplicationContext;

class CreateUserUseCaseTest extends TestCase
{
    const EXISTINGUSERNAME = "EXISTINGUSERNAME";
    const NOTEXISTENTUSERNAME = "NOTEXISTINGUSERNAME";

    private $gateway;
    private $gatewayMock;

    private $useCase;

    private $existingUser;
    private $nonExistentUser;

    private $presenter;

    public static function setUpBeforeClass()
    {
        ApplicationContext::initContext();
    }

    public function setUp()
    {
        $this->existingUser = new User(self::EXISTINGUSERNAME);
        $this->nonExistentUser = new User(self::EXISTINGUSERNAME);

        $this->gateway = ApplicationContext::$userGateway;
        $this->presenter = new BasicPresenter();

        $this->gatewayMock = Mockery::mock(UserGateway::class);
        $this->gatewayMock->allows()->findByUserName(self::EXISTINGUSERNAME)
            ->andReturns($this->existingUser);
        $this->gatewayMock->allows()->findByUserName(self::NOTEXISTENTUSERNAME)
            ->andReturns(null);

        $this->useCase = new CreateUserUseCase($this->gatewayMock, $this->presenter);
    }

    public function tearDown()
    {
        Mockery::close();
    }

    public function testWhenUserExistsErrorIsPresented()
    {
        $this->gatewayMock->allows()->save($this->existingUser)
            ->andThrow(DuplicateUserException::class);

        $this->useCase->execute(["user" => $this->existingUser]);

        $this->assertEquals("DUPLICATEUSER", $this->presenter->getError());
        $this->assertEmpty($this->presenter->getResponse());
    }

    public function testWhenInsertingUserAndGatewayThrowsInternalException_ErrorIsPresented()
    {
        $this->gatewayMock->allows()->save($this->existingUser)
            ->andThrow(\Exception::class);

        $this->useCase->execute(["user" => $this->existingUser]);

        $this->assertEquals("CREATEUSERERROR", $this->presenter->getError());
        $this->assertEmpty($this->presenter->getResponse());
    }

    public function testWhenUserDoesntExistNoErrorIsPresented()
    {
        $this->gatewayMock->allows()->save($this->nonExistentUser)
            ->andReturns($this->nonExistentUser);

        $this->useCase->execute(["user" => $this->nonExistentUser]);

        $this->assertNull($this->useCase->getPresenter()->getError());
    }

    public function testWhenUserDoesntExist_ItGetsInserted()
    {
        $this->useCase = new CreateUserUseCase(ApplicationContext::$userGateway, new BasicPresenter());

        $this->useCase->execute(["user" => $this->nonExistentUser]);

        $this->assertNotNull(ApplicationContext::$userGateway->findByUserName($this->nonExistentUser->getUserName()));
    }

}
