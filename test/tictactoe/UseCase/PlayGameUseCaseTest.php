<?php
declare(strict_types=1);

namespace TicTacToe;

use Mockery;
use PHPUnit\Framework\TestCase;
use TicTacToe\Engine\TicTactToeEngine;
use TicTacToe\Entity\GamePlayer;
use TicTacToe\Entity\User;
use TicTacToe\Exception\GameOverException;
use TicTacToe\Exception\PositionOccupiedException;
use TicTacToe\Exception\WaitYourTurnException;
use TicTacToe\UseCase\{
    BasicPresenter, PlayGameUseCase
};
use TicTacToe\Utility\ApplicationContext;
use TicTacToe\ValueObject\CircleShape;
use TicTacToe\ValueObject\CrossShape;
use TicTacToe\ValueObject\GameCoordinates;

class PlayGameUseCaseTest extends TestCase
{
    const SAMPLE_PLAYER_ONE_USERNAME = "john";
    const SAMPLE_PLAYER_TWO_USERNAME = "jack";

    const A_1 = "A1";
    const A_2 = "A2";
    const CROSS_SHAPE = "X";
    const CIRCLE_SHAPE = "O";

    private static $gameEngine;
    private static $circlePlayer;
    private static $crossPlayer;

    private $useCase;
    private $presenter;


    public static function setUpBeforeClass()
    {
        ApplicationContext::initContext();
        self::$circlePlayer = new GamePlayer(new CircleShape(), new User(self::SAMPLE_PLAYER_ONE_USERNAME));
        self::$crossPlayer = new GamePlayer(new CrossShape(), new User(self::SAMPLE_PLAYER_TWO_USERNAME));
    }

    private static function isCrossAtFirstPoitionOfFirstRow(string $matrix): bool
    {
        return substr($matrix, 0, 1) === self::CROSS_SHAPE;
    }

    private static function isCircleAtSecondPoitionOfFirstRow(string $matrix): bool
    {
        return substr($matrix, 0, 1) === self::CIRCLE_SHAPE;
    }

    public function setUp(): void
    {
        ApplicationContext::createGameEngine();
        self::$gameEngine = ApplicationContext::$gameEngine;
        self::$gameEngine->registerPlayers(self::$circlePlayer, self::$crossPlayer);
        self::$gameEngine->startGame();

        $this->presenter = new BasicPresenter();
        $this->useCase = new PlayGameUseCase($this->presenter, self::$gameEngine);
    }

    public function tearDown()
    {
        Mockery::close();
    }

    public function testCanMakeFirstMove()
    {
        $this->useCase->execute(["shape" => self::CROSS_SHAPE, "coordinate" => self::A_1]);

        $presenter = $this->useCase->getPresenter();
        $this->assertNotNull($presenter->getResponse());
        $this->assertNull($presenter->getError());
    }

    public function testWhenGameEngineThrowsWaitYourTurnException_ErrorIsPresentedAndNoMoveIsMade()
    {
        $gameEngineMock = \Mockery::mock(TicTactToeEngine::class)->shouldIgnoreMissing();
        $gameEngineMock->allows()->playCross()
            ->with(\Hamcrest\Matchers::equalTo(new GameCoordinates(GameCoordinates::A1)))
            ->andThrows(WaitYourTurnException::class);
        $useCase = new PlayGameUseCase($this->presenter, $gameEngineMock);

        $useCase->execute(["shape" => self::CROSS_SHAPE, "coordinate" => self::A_1]);

        $this->assertEquals("WAITYOURTURN", $useCase->getPresenter()->getError());
    }

    public function testWhenGameEngineThrowsPositionOccupiedException_ErrorIsPresentedAndNoMoveIsMade()
    {
        $gameEngineMock = \Mockery::mock(TicTactToeEngine::class)->shouldIgnoreMissing();
        $gameEngineMock->allows()->playCross()
            ->with(\Hamcrest\Matchers::equalTo(new GameCoordinates(GameCoordinates::A1)))
            ->andThrows(PositionOccupiedException::class);
        $useCase = new PlayGameUseCase($this->presenter, $gameEngineMock);

        $useCase->execute(["shape" => self::CROSS_SHAPE, "coordinate" => self::A_1]);

        $this->assertEquals("POSITIONOCCUPIED", $useCase->getPresenter()->getError());
    }

    public function testWhenGameEngineThrowsGameAlreadyOverException_ErrorIsPresentedAndNoMoveIsMade()
    {
        $gameEngineMock = \Mockery::mock(TicTactToeEngine::class)->shouldIgnoreMissing();
        $gameEngineMock->allows()->playCross()
            ->with(\Hamcrest\Matchers::equalTo(new GameCoordinates(GameCoordinates::A1)))
            ->andThrows(GameOverException::class);
        $useCase = new PlayGameUseCase($this->presenter, $gameEngineMock);

        $useCase->execute(["shape" => self::CROSS_SHAPE, "coordinate" => self::A_1]);

        $this->assertEquals("GAMEALREADYOVER", $useCase->getPresenter()->getError());
    }

    public function testWhenCrossMakesMoveInA1_MoveIsMadeAndResponseContainsMatrixUpdateAccordingly()
    {
        $this->useCase->execute(["shape" => self::CROSS_SHAPE, "coordinate" => self::A_1]);

        $this->assertNull($this->useCase->getPresenter()->getError());
        $this->assertTrue(self::isCrossAtFirstPoitionOfFirstRow($this->useCase->getPresenter()->getResponse()["matrix"]));
    }

    public function testWhenCircleMakesMoveInA1_MoveIsMadeAndResponseContainsMatrixUpdateAccordingly()
    {
        $this->useCase->execute(["shape" => self::CIRCLE_SHAPE, "coordinate" => self::A_1]);

        $this->assertNull($this->useCase->getPresenter()->getError());
        $this->assertTrue(self::isCircleAtSecondPoitionOfFirstRow($this->useCase->getPresenter()->getResponse()["matrix"]));
    }
}
