<?php
declare(strict_types=1);

namespace TicTacToe\UseCase;

use PHPUnit\Framework\TestCase;

class NotImplementedUseCaseTest extends TestCase
{

    public function testWhenInvoked_ErrorIsPresented()
    {
        $useCase = new NotImplementedUseCase(new BasicPresenter());
        $useCase->execute();

        $this->assertEquals("NOTIMPLEMENTEDUSECASE", $useCase->getPresenter()->getError());
        $this->assertNull($useCase->getPresenter()->getResponse());
    }
}
