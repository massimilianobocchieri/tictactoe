<?php
declare(strict_types=1);

namespace TicTacToe\UseCase;

use Mockery;
use PHPUnit\Framework\TestCase;
use TicTacToe\Entity\User;
use TicTacToe\Exception\DuplicateUserException;
use TicTacToe\Exception\UserNotFoundException;
use TicTacToe\Gateway\UserGateway;
use TicTacToe\Utility\ApplicationContext;

class DeleteUserUseCaseTest extends TestCase
{
    const EXISTINGUSERNAME = "EXISTINGUSERNAME";
    const NOTEXISTENTUSERNAME = "NOTEXISTINGUSERNAME";

    private $gateway;
    private $gatewayMock;

    private $useCase;

    private $existingUser;
    private $nonExistentUser;

    private $presenter;

    public static function setUpBeforeClass()
    {
        ApplicationContext::initContext();
    }

    public function setUp()
    {
        $this->existingUser = new User(self::EXISTINGUSERNAME);
        $this->nonExistentUser = new User(self::EXISTINGUSERNAME);

        $this->gateway = ApplicationContext::$userGateway;
        $this->presenter = new BasicPresenter();

        $this->gatewayMock = Mockery::mock(UserGateway::class);
        $this->gatewayMock->allows()->findByUserName(self::EXISTINGUSERNAME)
            ->andReturns($this->existingUser);
        $this->gatewayMock->allows()->findByUserName(self::NOTEXISTENTUSERNAME)
            ->andReturns(null);

        $this->useCase = new DeleteUserUseCase($this->gatewayMock, $this->presenter);
    }

    public function tearDown()
    {
        Mockery::close();
    }

    public function testWhenUserDoesntExist_ErrorIsPresented()
    {
        $this->gatewayMock->allows()->delete($this->nonExistentUser)
            ->andThrow(UserNotFoundException::class);

        $this->useCase->execute(["user" => $this->nonExistentUser]);

        $this->assertEquals("USERNOTFOUND", $this->presenter->getError());
        $this->assertEmpty($this->presenter->getResponse());
    }

    public function testWhenDeletingUserAndGatewayThrowsInternalException_ErrorIsPresented()
    {
        $this->gatewayMock->allows()->delete($this->existingUser)
            ->andThrow(\Exception::class);

        $this->useCase->execute(["user" => $this->existingUser]);

        $this->assertEquals("DELETEUSERERROR", $this->presenter->getError());
        $this->assertEmpty($this->presenter->getResponse());
    }

    public function testWhenUserExistNoErrorIsPresented()
    {
        $this->gatewayMock->allows()->delete($this->existingUser)
            ->andReturns($this->existingUser);

        $this->useCase->execute(["user" => $this->existingUser]);

        $this->assertNull($this->useCase->getPresenter()->getError());
    }

    public function testWhenUserExists_ItGetsDelete()
    {
        $this->useCase = new DeleteUserUseCase(ApplicationContext::$userGateway, new BasicPresenter());

        $this->useCase->execute(["user" => $this->existingUser]);

        $this->assertNull(ApplicationContext::$userGateway->findByUserName($this->existingUser->getUserName()));
    }

}
