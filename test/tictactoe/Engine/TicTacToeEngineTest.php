<?php
declare(strict_types=1);

namespace TicTacToe\Engine;


use PHPUnit\Framework\TestCase;
use TicTacToe\{
    Entity\Game,
    Entity\GamePlayer,
    Entity\User,
    ValueObject\CircleShape,
    ValueObject\CrossShape,
    ValueObject\GameCoordinates
};

class TicTacToeEngineTest extends TestCase
{
    private static $SAMPLE_CIRCLE_PLAYER, $SAMPLE_CROSS_PLAYER;
    private $gameEngine;
    private $game;

    public static function setUpBeforeClass()
    {
        self::$SAMPLE_CIRCLE_PLAYER = new GamePlayer(new CircleShape(), new User("john"));
        self::$SAMPLE_CROSS_PLAYER = new GamePlayer(new CrossShape(), new User("john"));
    }

    public function setUp()
    {
        $this->game = new Game();
        $this->gameEngine = new TicTactToeEngineImpl($this->game);
    }

    public function testWhenGameEngineIsInstantiatedGameStateHolderEntityIsProperlySet()
    {
        $this->assertEquals($this->game, $this->gameEngine->getGame());
    }

    /**
     * @expectedException TicTacToe\Exception\PlayersNotRegisteredException
     */
    public function testWhenGameIsStartedAndPlayersAreNotRegistered_ExceptionIsThrown()
    {
        $this->gameEngine->startGame();
    }

    /**
     * @expectedException TicTacToe\Exception\PlayersAlreadyRegisteredException
     */
    public function testWhenRegisteringAlreadyRegisterPlayers_ExceptionIsThrown()
    {
        $this->gameEngine->registerPlayers(self::$SAMPLE_CIRCLE_PLAYER, self::$SAMPLE_CROSS_PLAYER);

        $this->gameEngine->registerPlayers(self::$SAMPLE_CIRCLE_PLAYER, self::$SAMPLE_CROSS_PLAYER);
    }

    /**
     * @expectedException TicTacToe\Exception\WrongShapeException
     */
    public function testWhenRegisteringPlayersWithWrongShape_ExceptionIsThrown()
    {
        $this->gameEngine->registerPlayers(self::$SAMPLE_CROSS_PLAYER, self::$SAMPLE_CIRCLE_PLAYER);
    }

    /**
     * @expectedException TicTacToe\Exception\GameCannotRestartException
     */
    public function testWhenStartingAlreadyStartedGame_ExceptionIsThrown()
    {
        $this->gameEngine->registerPlayers(self::$SAMPLE_CIRCLE_PLAYER, self::$SAMPLE_CROSS_PLAYER);
        $this->gameEngine->startGame();

        $this->gameEngine->startGame();
    }

    /**
     * @expectedException TicTacToe\Exception\PlayersNotRegisteredException
     */
    public function testWithPlayersNotRegisterPlayActionIsMade_ExceptionIsThrown()
    {
        $this->gameEngine->playCross(new GameCoordinates(GameCoordinates::A1));
    }

    /**
     * @expectedException TicTacToe\Exception\WaitYourTurnException
     */
    public function testWhenPlayingTwiceSamePlayer_ExceptionIsThrown()
    {
        $this->gameEngine->registerPlayers(self::$SAMPLE_CIRCLE_PLAYER, self::$SAMPLE_CROSS_PLAYER);
        $this->gameEngine->startGame();

        $this->gameEngine->playCross(new GameCoordinates(GameCoordinates::A1));
        $this->gameEngine->playCross(new GameCoordinates(GameCoordinates::A2));
    }

    /**
     * @expectedException TicTacToe\Exception\GameOverException
     */
    public function testGameOver()
    {
        $this->gameEngine->registerPlayers(self::$SAMPLE_CIRCLE_PLAYER, self::$SAMPLE_CROSS_PLAYER);
        $this->gameEngine->startGame();

        $this->crossWinsSample();
        $this->gameEngine->playCircle(new GameCoordinates(GameCoordinates::C3));
    }

    public function testWhenGameStarted_PlayersAreRegistered()
    {
        $this->gameEngine->registerPlayers(self::$SAMPLE_CIRCLE_PLAYER, self::$SAMPLE_CROSS_PLAYER);
        $this->gameEngine->startGame();

        $this->assertEquals(self::$SAMPLE_CROSS_PLAYER, $this->gameEngine->getCrossPlayer());
        $this->assertEquals(self::$SAMPLE_CIRCLE_PLAYER, $this->gameEngine->getCirclePlayer());
        $this->assertEquals("STARTED", $this->game->getStatus()->getValue());
    }

    public function testWhenStillPlaying_NoWinnerYet()
    {
        $this->gameEngine->registerPlayers(self::$SAMPLE_CIRCLE_PLAYER, self::$SAMPLE_CROSS_PLAYER);
        $this->gameEngine->startGame();

        $this->gameEngine->playCross(new GameCoordinates(GameCoordinates::A1));

        $this->assertNull($this->gameEngine->checkWinner());
        $this->assertEquals("STARTED", $this->game->getStatus()->getValue());
    }

    public function testTie()
    {
        $this->gameEngine->registerPlayers(self::$SAMPLE_CIRCLE_PLAYER, self::$SAMPLE_CROSS_PLAYER);
        $this->gameEngine->startGame();

        $this->tieSample();

        $this->assertNull($this->gameEngine->checkWinner());
        $this->assertEquals("FINISHED", $this->game->getStatus()->getValue());
        $this->assertEquals("TIE", $this->game->getWinner()->getValue());
        $this->assertTrue($this->gameEngine->isTie());

        $this->printMatrix();
    }

    public function testCrossWins()
    {
        $this->gameEngine->registerPlayers(self::$SAMPLE_CIRCLE_PLAYER, self::$SAMPLE_CROSS_PLAYER);
        $this->gameEngine->startGame();

        $this->crossWinsSample();

        $this->assertEquals(self::$SAMPLE_CROSS_PLAYER, $this->gameEngine->checkWinner());
        $this->assertEquals("FINISHED", $this->game->getStatus()->getValue());
        $this->assertEquals("CROSS", $this->game->getWinner()->getValue());
        $this->printMatrix();
    }

    public function testCircleWins()
    {
        $this->gameEngine->registerPlayers(self::$SAMPLE_CIRCLE_PLAYER, self::$SAMPLE_CROSS_PLAYER);
        $this->gameEngine->startGame();

        $this->circleWinsSample();

        $this->assertEquals(self::$SAMPLE_CIRCLE_PLAYER, $this->gameEngine->checkWinner());
        $this->assertEquals("FINISHED", $this->game->getStatus()->getValue());
        $this->assertEquals("CIRCLE", $this->game->getWinner()->getValue());
        $this->printMatrix();
    }

    public function testWinnerInAColumn()
    {
        $this->gameEngine->registerPlayers(self::$SAMPLE_CIRCLE_PLAYER, self::$SAMPLE_CROSS_PLAYER);
        $this->gameEngine->startGame();

        $this->winnerInAColumn();

        $this->assertEquals("FINISHED", $this->game->getStatus()->getValue());
        $this->printMatrix();
    }

    public function testWinnerInDiagonal()
    {
        $this->gameEngine->registerPlayers(self::$SAMPLE_CIRCLE_PLAYER, self::$SAMPLE_CROSS_PLAYER);
        $this->gameEngine->startGame();

        $this->winnerDiagonal();

        $this->assertEquals("FINISHED", $this->game->getStatus()->getValue());
        $this->printMatrix();
    }

    public function testWinnerInSecondaryDiagonal()
    {
        $this->gameEngine->registerPlayers(self::$SAMPLE_CIRCLE_PLAYER, self::$SAMPLE_CROSS_PLAYER);
        $this->gameEngine->startGame();

        $this->winnerSecondaryDiagonal();

        $this->assertEquals("FINISHED", $this->game->getStatus()->getValue());
        $this->printMatrix();
    }

    public function testCrossWinsAtLastTurnWithFullMatrix()
    {
        $this->gameEngine->registerPlayers(self::$SAMPLE_CIRCLE_PLAYER, self::$SAMPLE_CROSS_PLAYER);
        $this->gameEngine->startGame();

        $this->crossWinsLastTurnSample();

        $this->assertEquals("FINISHED", $this->game->getStatus()->getValue());
        $this->assertEquals("CROSS", $this->game->getWinner()->getValue());
        $this->printMatrix();
    }

    private function tieSample(): void
    {
        $this->gameEngine->playCross(new GameCoordinates(GameCoordinates::A1));
        $this->gameEngine->playCircle(new GameCoordinates(GameCoordinates::B1));
        $this->gameEngine->playCross(new GameCoordinates(GameCoordinates::A2));
        $this->gameEngine->playCircle(new GameCoordinates(GameCoordinates::B2));
        $this->gameEngine->playCross(new GameCoordinates(GameCoordinates::B3));
        $this->gameEngine->playCircle(new GameCoordinates(GameCoordinates::A3));
        $this->gameEngine->playCross(new GameCoordinates(GameCoordinates::C1));
        $this->gameEngine->playCircle(new GameCoordinates(GameCoordinates::C2));
        $this->gameEngine->playCross(new GameCoordinates(GameCoordinates::C3));
    }

    private function crossWinsSample(): void
    {
        $this->gameEngine->playCross(new GameCoordinates(GameCoordinates::A1));
        $this->gameEngine->playCircle(new GameCoordinates(GameCoordinates::B1));
        $this->gameEngine->playCross(new GameCoordinates(GameCoordinates::A2));
        $this->gameEngine->playCircle(new GameCoordinates(GameCoordinates::B2));
        $this->gameEngine->playCross(new GameCoordinates(GameCoordinates::A3));
    }

    private function circleWinsSample(): void
    {
        $this->gameEngine->playCross(new GameCoordinates(GameCoordinates::B1));
        $this->gameEngine->playCircle(new GameCoordinates(GameCoordinates::A1));
        $this->gameEngine->playCross(new GameCoordinates(GameCoordinates::B2));
        $this->gameEngine->playCircle(new GameCoordinates(GameCoordinates::A2));
        $this->gameEngine->playCross(new GameCoordinates(GameCoordinates::C1));
        $this->gameEngine->playCircle(new GameCoordinates(GameCoordinates::A3));
    }

    private function crossWinsLastTurnSample(): void
    {
        $this->gameEngine->playCross(new GameCoordinates(GameCoordinates::A1));
        $this->gameEngine->playCircle(new GameCoordinates(GameCoordinates::A2));
        $this->gameEngine->playCross(new GameCoordinates(GameCoordinates::B2));
        $this->gameEngine->playCircle(new GameCoordinates(GameCoordinates::B1));
        $this->gameEngine->playCross(new GameCoordinates(GameCoordinates::A3));
        $this->gameEngine->playCircle(new GameCoordinates(GameCoordinates::C1));
        $this->gameEngine->playCross(new GameCoordinates(GameCoordinates::C2));
        $this->gameEngine->playCircle(new GameCoordinates(GameCoordinates::B3));
        $this->gameEngine->playCross(new GameCoordinates(GameCoordinates::C3));
    }

    private function winnerInAColumn(): void
    {
        $this->gameEngine->playCross(new GameCoordinates(GameCoordinates::A1));
        $this->gameEngine->playCircle(new GameCoordinates(GameCoordinates::A2));
        $this->gameEngine->playCross(new GameCoordinates(GameCoordinates::B1));
        $this->gameEngine->playCircle(new GameCoordinates(GameCoordinates::B2));
        $this->gameEngine->playCross(new GameCoordinates(GameCoordinates::C1));
    }

    private function winnerDiagonal(): void
    {
        $this->gameEngine->playCross(new GameCoordinates(GameCoordinates::A1));
        $this->gameEngine->playCircle(new GameCoordinates(GameCoordinates::A2));
        $this->gameEngine->playCross(new GameCoordinates(GameCoordinates::B2));
        $this->gameEngine->playCircle(new GameCoordinates(GameCoordinates::A3));
        $this->gameEngine->playCross(new GameCoordinates(GameCoordinates::C3));
    }

    private function winnerSecondaryDiagonal(): void
    {
        $this->gameEngine->playCross(new GameCoordinates(GameCoordinates::A3));
        $this->gameEngine->playCircle(new GameCoordinates(GameCoordinates::A1));
        $this->gameEngine->playCross(new GameCoordinates(GameCoordinates::B2));
        $this->gameEngine->playCircle(new GameCoordinates(GameCoordinates::B1));
        $this->gameEngine->playCross(new GameCoordinates(GameCoordinates::C1));
    }

    private function printMatrix(): void
    {
        echo $this->game->getGameMatrix() . PHP_EOL . PHP_EOL;
    }
}