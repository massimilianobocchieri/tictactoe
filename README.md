Tic Tac Toe Code Challenge Solution
========
Tic Tac Toe Game Use Cases

See Code Challenge Specifications [CODECHALLENGE.md](CODECHALLENGE.md)


# Requirements

The following software dependencies are required to run this project:

- PHP >= 7.2
- PHPUnit >= 7.3.5
- Composer >= 1.7.2

# Installation

This project is using the PHP package manager `composer`:
- https://getcomposer.org/doc/01-basic-usage.md#installing-dependencies


To install the project dependencies, please run, under the project root folder:

 `composer install`

# Unit Test and coverage

From the project root run 

 1- `php ./vendor/phpunit/phpunit/phpunit --configuration ./phpunit.xml --teamcity`
 
Coverage reports will be available in the  **build** folder


# Application Start (CommandLine console UserInterface)

From the project root run in Terminal

`php ./console.php`


# Frameworks and Libraries

  - Enum library for a more "structured" values enumeration checks
  - Mockery for double/Mocking

# Assumptions about code challenge requirements 

I have assumed that the word user has two "roles" in the requirements

 - For Add/Delete Users Scenario: I create a more anagrafic User Entity (@ the moment having username as Id and unique field, but extensible without spread impact)
 - For The Game Scenarios I assumed the GamePlayer entity who (using composition) has a User entity related to It.

# Architecture, Design Patterns and considerations

The source code of the project has been structured in a "flat" way following  in part the Principle of Clean Code (by Uncle Bob)
with special attention to UseCases, the core of the application and the Game Engine.

    Design Patterns used
    
    - Abstract Factory (to create Use Cases decoupling interface from concrete implementations)
    - Command (for Use Cases)
    - Dependency Inversion to allow loose coupling and multiple strategies (i.e for ticTacToe Engine)
 
 To Mark architecture boundaries in order to separate business logic (core, use cases, Game Engine and Model/Entities) from UserInterface and Persistence, Interfaces have been used as much as possible
 also to facilitate Unit Testing/Mocking.
 
 Encapsulation (i.e for entities has been used, transporting Entity objects instead of row data especially in the UseCase layer) has been used to allow future fields addition (See NextGenerationUser and User Class) without affecting all the boundaries
 and to allow future changes (considering that challenge requisites are quite vague) without breaking all code and avoid code rigidity.
 
 I have tried to follow SOLID Principles (Single Responsiblity, Open Closed Principle with Abstract Classes and Interfaces, Liskov Substitution Principle with subclasses respecting the parent contract, Interface Segregation creating minimal Interfaces related to 
 roles/responsibilities and allowing concrete classes to possibly implement multiple interfaces, Dependency Inversion Principle(avoiding business logic depend on external boundaries like UI and Persistence)

With the implemented architecture, being Core/UseCases decoupled from the rest, should be easy create an API (i.e a Rest one) just implementing Controllers and dispatcher and using the existing UseCases.
Same is for persistence, implementing gateway interfaces and inject them in the Main/Controllers without much effort.  

For lack of time I have decided to test only classes with more logic (Use Cases and Game Engine) following a partial TDD approach and more upfront unit test.

A note about the ApplicationContext class, bootstrapped in the main App (Console Class): it has been used to allow a sort of Persistence/Context of the system, to store and maintain data for users and game angine
even if core application is not tied to it.


Game info are stored in the Game class, handled by Engine during Game Play and in the future can be translated and persisted without much effort.

The Play move scenario is dependent on the Start new game one, and after each move the matrix is printed, but the result is not intentionally shown, you must press 1 to show it starting the Check Game Status UseCase.

If it has to behave differently it can be made simply by asking the controller to invoke the check game status use case before rendering the response to the console.

#TODO

 - Refactor (In the Game Engine implementation class there is a bit of confusion and some code envy smell even if the coupling between the Engine and the Game class is acceptable and game is used as state holder, "handled" by the Engine acting as a manager)
 - Create more tests, possibly in the Controller Layer
 - Use some BDD (possibly with Behat) to have a business and complete view of the architecture scenarios: I have already used Behat previously for other REST based applications 
 but I didn't find quickly a way to handle with Console input/output based scenarios.
 
 