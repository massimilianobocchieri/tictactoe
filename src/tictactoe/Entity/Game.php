<?php
declare(strict_types=1);

namespace TicTacToe\Entity;


use TicTacToe\Engine\GameMatrix;
use TicTacToe\ValueObject\{
    GameStatus, GameTurn, GameWinner
};

class Game extends Entity
{
    protected $currentTurn;
    protected $winner;
    private $status;
    private $crossPlayer;
    private $circlePlayer;
    private $gameMatrix;

    public function __construct()
    {
        parent::__construct();
    }

    public function getStatus(): ?GameStatus
    {
        return $this->status;
    }

    public function setStatus(GameStatus $status): void
    {
        $this->status = $status;
    }

    public function getCrossPlayer(): ?GamePlayer
    {
        return $this->crossPlayer;
    }

    public function setCrossPlayer(GamePlayer $crossPlayer): void
    {
        $this->crossPlayer = $crossPlayer;
    }

    public function getCirclePlayer(): ?GamePlayer
    {
        return $this->circlePlayer;
    }

    public function setCirclePlayer(GamePlayer $circlePlayer): void
    {
        $this->circlePlayer = $circlePlayer;
    }

    public function getGameMatrix(): GameMatrix
    {
        return $this->gameMatrix;
    }

    public function setGameMatrix(GameMatrix $gameMatrix): void
    {
        $this->gameMatrix = $gameMatrix;
    }

    public function getCurrentTurn(): GameTurn
    {
        return $this->currentTurn;
    }

    public function setCurrentTurn(GameTurn $currentTurn): void
    {
        $this->currentTurn = $currentTurn;
    }

    /**
     * @return mixed
     */
    public function getWinner(): GameWinner
    {
        return $this->winner;
    }

    public function setWinner(GameWinner $winner): void
    {
        $this->winner = $winner;
    }


}