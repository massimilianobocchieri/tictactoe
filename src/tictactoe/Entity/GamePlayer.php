<?php
declare(strict_types=1);

namespace TicTacToe\Entity;


use TicTacToe\ValueObject\{
    GameShape
};

class GamePlayer extends Entity
{
    private $shape;
    private $user;

    public function __construct(GameShape $shape, User $user)
    {
        parent::__construct();

        $this->shape = $shape;
        $this->user = $user;
    }

    public function getShape(): GameShape
    {
        return $this->shape;
    }

    public function getUser(): User
    {
        return $this->user;
    }

}