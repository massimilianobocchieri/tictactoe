<?php
declare(strict_types=1);

namespace TicTacToe\Entity;


class User extends Entity
{
    private $userName;

    public function __construct($userName)
    {
        parent::__construct();
        $this->userName = $userName;
    }

    public function getUserName(): string
    {
        return $this->userName;
    }

    public function __toString()
    {
        return "I am {$this->userName}";
    }

    public function isSame(Entity $anotherUser): bool
    {
        return $anotherUser instanceof User && $this->userName == $anotherUser->getUserName();
    }
}