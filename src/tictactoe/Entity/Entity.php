<?php
declare(strict_types=1);

namespace TicTacToe\Entity;


abstract class Entity
{
    private static $IdSequence;

    private $id;

    protected function __construct()
    {
        $this->id = $this->makeId();
    }

    protected function makeId(): int
    {
        return $this->nextId();
    }

    protected function nextId(): int
    {
        return ++self::$IdSequence;
    }

    public function isSame(Entity $anotherUser): bool
    {
        return $this->id() === $anotherUser->id();
    }

    public function id(): int
    {
        return $this->id;
    }

}
