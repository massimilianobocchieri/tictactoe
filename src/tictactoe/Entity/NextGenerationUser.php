<?php
declare(strict_types=1);

namespace TicTacToe\Entity;


class NextGenerationUser extends User
{
    protected $lastName;
    protected $firstName;

    public function __construct(string $userName, string $lastName, string $firstName)
    {
        parent::__construct($userName);
        $this->lastName = $lastName;
        $this->firstName = $firstName;
    }

}