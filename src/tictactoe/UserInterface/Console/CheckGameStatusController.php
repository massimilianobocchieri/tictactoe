<?php
declare(strict_types=1);

namespace TicTacToe\UserInterface\Console;


use TicTacToe\ApplicationContext;
use TicTacToe\UseCase\ResponseInterface;
use TicTacToe\UseCase\UseCaseInterface;

class CheckGameStatusController
{
    private $useCase;
    private $response;

    public function __construct(UseCaseInterface $useCase, ResponseInterface $response)
    {
        $this->useCase = $useCase;
        $this->response = $response;
    }

    public function render(): ResponseInterface
    {
        $this->useCase->execute();
        $presenter = $this->useCase->getPresenter();

        $this->formatResponse($presenter->getResponse());

        return $this->response;
    }

    private function formatResponse(array $presenterResponse)
    {
        $message = "";
        if ($presenterResponse['status'] == 'FINISHED') {
            $message = "Game is Over";
            if ($presenterResponse['isTie'] == true)
                $message .= " the play went TIE!";
            else
                $message .= " and the winner is:" . $presenterResponse["winner"];
        } else {
            $message = "The game is still in progress...";
        }

        $this->response->setData(["message" => $message]);
    }
}