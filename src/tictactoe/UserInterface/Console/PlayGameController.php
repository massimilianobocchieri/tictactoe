<?php
declare(strict_types=1);

namespace TicTacToe\UserInterface\Console;


use TicTacToe\UseCase\ResponseInterface;
use TicTacToe\UseCase\UseCaseInterface;

class PlayGameController
{
    private $useCase;
    private $response;

    public function __construct(UseCaseInterface $useCase, ResponseInterface $response)
    {
        $this->useCase = $useCase;
        $this->response = $response;
    }

    public function render(string $shape, string $coordinate): ResponseInterface
    {
        $this->useCase->execute(['shape' => $shape, "coordinate" => $coordinate]);
        $presenter = $this->useCase->getPresenter();

        if ($presenter->getError() != null)
            $this->formatErrorResponse($presenter->getError(), $presenter->getResponse());
        else
            $this->formatResponse($presenter->getResponse());;

        return $this->response;
    }

    private function formatErrorResponse(string $error, array $presenterResponse)
    {
        switch ($error) {
            case "GAMEALREADYOVER":
                $this->response->setStatusCode(1);
                $errorMessage = "Game is already Over...";
                break;
            case "WAITYOURTURN":
                $this->response->setStatusCode(2);
                $errorMessage = "It's not your turn to move...";
                break;
            case "POSITIONOCCUPIED":
                $this->response->setStatusCode(3);
                $errorMessage = "Coordinate already occupied...";
                break;
            default:
                $this->response->setStatusCode(99);
                $errorMessage = "Ops..something went wrong...";
                break;
        }

        $this->response->setData(["message" => $presenterResponse["matrix"], "errorMessage" => $errorMessage]);
    }

    private function formatResponse(array $presenterResponse)
    {
        $this->response->setData(["message" => $presenterResponse["matrix"]]);
    }
}