<?php
declare(strict_types=1);

namespace TicTacToe\UserInterface\Console;


use TicTacToe\Utility\ApplicationContext;

class Console extends AbstractConsole
{
    private const EXIT_CHOICE = 5;

    private const MENU_CHOICES = [
        1 => "Create User",
        2 => "Delete User",
        3 => "List Users",
        4 => "Start a New Game between two Players",
        self::EXIT_CHOICE => "Exit the Application"
    ];

    public function execute(?array $request = []): void
    {
        ApplicationContext::initContext();

        do {
            $this->showMenu();
            $choice = (int)readline("Please make Your choice and press enter...");

            $this->handleChoice($choice);

        } while ($choice != self::EXIT_CHOICE);
    }


    private function showMenu(): void
    {
        echo "Tic Tac Toe Application Console" . PHP_EOL . PHP_EOL;

        foreach (self::MENU_CHOICES as $choice => $description)
            echo "{$choice} - {$description}" . PHP_EOL;

        echo PHP_EOL;
    }

    private function handleChoice(int $choice): void
    {
        switch ($choice) {
            case 1:
                $console = new CreateUserConsole();
                $console->execute();
                break;
            case 2:
                $console = new DeleteUserConsole();
                $console->execute();
                break;
            case 3:
                $console = new ListUsersConsole();
                $console->execute();
                break;
            case 4:
                $console = new StartNewGameConsole();
                $console->execute();
                break;
            case self::EXIT_CHOICE:
            default:
                break;
        }
    }

}