<?php
declare(strict_types=1);

namespace TicTacToe\UserInterface\Console;


class CreateUserConsole extends AbstractConsole
{
    public function execute(?array $request = []): void
    {
        do
            $inputLine = readline("Insert the username of the user to create (allowed only letters and numbers, max size 10 chars) and press Enter...");
        while (!$this->isValid($inputLine));

        $controller = new CreateUserController($this->useCaseFactory->makeUseCase(['choice' => 1]), new BasicResponse());
        $response = $controller->render($inputLine);

        if ($response->getStatusCode() == BasicResponse::OK_STATUS_CODE)
            $this->printLine($response->getData()["message"]);
        else
            $this->printLine("CAUTION:" . $response->getData()["errorMessage"] . "!!!");

    }

    private function isValid(?string $input = null): bool
    {
        return preg_match("/^[a-z][a-z0-9]{0,9}$/", $input) != FALSE;
    }
}