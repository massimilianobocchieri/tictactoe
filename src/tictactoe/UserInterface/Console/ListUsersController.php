<?php
declare(strict_types=1);

namespace TicTacToe\UserInterface\Console;

use TicTacToe\UseCase\ResponseInterface;
use TicTacToe\UseCase\UseCaseInterface;

class ListUsersController
{
    private $useCase;
    private $response;

    public function __construct(UseCaseInterface $useCase, ResponseInterface $response)
    {
        $this->useCase = $useCase;
        $this->response = $response;
    }

    public function render(): ResponseInterface
    {
        $this->useCase->execute();
        $presenter = $this->useCase->getPresenter();

        $this->formatResponse($presenter->getResponse());;

        return $this->response;
    }

    private function formatResponse(array $presenterResponse)
    {
        $userNames = [];

        foreach ($presenterResponse["data"] as $user)
            $userNames[] = $user->getUserName();

        $this->response->setData(["data" => $userNames]);
    }
}