<?php
declare(strict_types=1);

namespace TicTacToe\UserInterface\Console;


class PlayGameConsole extends AbstractConsole
{
    private const ROLES = ["X", "0"];
    private const POSITIONS = ["A1", "A2", "A3", "B1", "B2", "B3", "C1", "C2", "C3"];

    public function execute(?array $request = []): void
    {
        $turn = $this->initTurnRandom();

        $this->printLine("Make your Moves");

        do {
            $inputMessage = "Turn for Player with [" . self::ROLES[$turn] . "], Please choose your move in range:" . join(",", self::POSITIONS) . "...., or 1 to check Results or 9 to exit:";
            $inputLine = readline($inputMessage);

            if ((int)$inputLine === 9)
                break;

            if ((int)$inputLine == 1) {
                $this->printLine("Check Game Results");
                $controller = new CheckGameStatusController($this->useCaseFactory->makeUseCase(['choice' => 6]), new BasicResponse());
                $response = $controller->render();

                $this->printLine($response->getData()["message"]);

                continue;
            }

            $choice = strtoupper($inputLine);

            if (!$this->isValid($choice)) {
                $this->printLine("Wrong Entry, please retry with correct value");
                continue;
            }

            $controller = new PlayGameController($this->useCaseFactory->makeUseCase(['choice' => 5]), new BasicResponse());
            $response = $controller->render(self::ROLES[$turn], $choice);

            $this->printLine($response->getData()["message"]);

            if ($response->getStatusCode() == BasicResponse::OK_STATUS_CODE)
                $turn = ($turn + 1) % 2;
            else
                $this->printLine("CAUTION:" . $response->getData()["errorMessage"] . "!!!");

        } while (true);
    }

    private function initTurnRandom(): int
    {
        return mt_rand(0, count(self::ROLES) - 1);
    }

    private function isValid(?string $choice = "WRONGCHOICE"): bool
    {
        return in_array($choice, self::POSITIONS);
    }
}