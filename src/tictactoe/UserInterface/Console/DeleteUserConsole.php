<?php
declare(strict_types=1);

namespace TicTacToe\UserInterface\Console;


class DeleteUserConsole extends AbstractConsole
{
    public function execute(?array $request = []): void
    {
        do
            $inputLine = readline("Insert the username of the user to delete and press Enter...");
        while (!$this->isValid($inputLine));

        $controller = new DeleteUserController($this->useCaseFactory->makeUseCase(['choice' => 2]), new BasicResponse());
        $response = $controller->render($inputLine);

        if ($response->getStatusCode() == BasicResponse::OK_STATUS_CODE)
            $this->printLine($response->getData()["message"]);
        else
            $this->printLine("CAUTION:" . $response->getData()["errorMessage"] . "!!!");
    }

    private function isValid(?string $input = null): bool
    {
        return preg_match("/^[a-z][a-z0-9]+$/", $input) != FALSE;
    }
}