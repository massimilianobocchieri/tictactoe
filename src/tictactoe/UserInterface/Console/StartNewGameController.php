<?php
declare(strict_types=1);

namespace TicTacToe\UserInterface\Console;

use TicTacToe\UseCase\ResponseInterface;
use TicTacToe\UseCase\UseCaseInterface;

class StartNewGameController
{
    private $useCase;
    private $response;

    public function __construct(UseCaseInterface $useCase, ResponseInterface $response)
    {
        $this->useCase = $useCase;
        $this->response = $response;
    }

    public function render(string $playerOneUseName, string $playerTwoUserName): ResponseInterface
    {
        $this->useCase->execute(['playerOne' => $playerOneUseName, 'playerTwo' => $playerTwoUserName]);
        $presenter = $this->useCase->getPresenter();

        $this->formatResponse($presenter->getResponse());
        return $this->response;
    }

    private function formatResponse(array $presenterReponse): void
    {
        $message =
            "Game started between users:" . $presenterReponse["crossUser"]["name"] . "(" . $presenterReponse["crossUser"]["shape"] . ") and "
            . $presenterReponse["circleUser"]["name"] . "(" . $presenterReponse["circleUser"]["shape"] . ")";

        $this->response->setData(["message" => $message]);
    }
}