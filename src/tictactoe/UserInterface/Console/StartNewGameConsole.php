<?php
declare(strict_types=1);

namespace TicTacToe\UserInterface\Console;


class StartNewGameConsole extends AbstractConsole
{
    public function execute(?array $request = []): void
    {
        do {
            $inputLine = readline("Insert username of player1 and player2 separated by space (stubbed values: john jack) and press Enter...");
            $inputItems = explode(" ", $inputLine);
        } while (!$this->isValid($inputItems));

        $controller = new StartNewGameController($this->useCaseFactory->makeUseCase(['choice' => 4]), new BasicResponse());
        $response = $controller->render($inputItems[0], $inputItems[1]);

        $this->printLine($response->getData()["message"]);

        $console = new PlayGameConsole();
        $console->execute();
    }

    private function isValid(?array $inputItems = []): bool
    {
        return ($inputItems && is_array($inputItems) && count($inputItems) == 2);
    }
}