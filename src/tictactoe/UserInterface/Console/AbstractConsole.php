<?php
declare(strict_types=1);

namespace TicTacToe\UserInterface\Console;


use TicTacToe\UseCase\GameUseCaseFactory;
use TicTacToe\UseCase\UseCaseFactoryInterface;

abstract class AbstractConsole
{
    protected $useCaseFactory;

    public function __construct(?UseCaseFactoryInterface $useCaseFactory = null)
    {
        $this->useCaseFactory = $useCaseFactory ?? new GameUseCaseFactory();
    }

    public abstract function execute(?array $request = []): void;

    protected function printLine(?string $message = null): void
    {
        echo $message . PHP_EOL;
    }

}