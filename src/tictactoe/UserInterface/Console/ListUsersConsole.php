<?php
declare(strict_types=1);

namespace TicTacToe\UserInterface\Console;


class ListUsersConsole extends AbstractConsole
{
    public function execute(?array $request = []): void
    {
        $this->printLine("Users List");
        $this->printLine();

        $controller = new ListUsersController($this->useCaseFactory->makeUseCase(['choice' => 3]), new BasicResponse());
        $response = $controller->render();

        foreach ($response->getData()["data"] as $user)
            $this->printLine($user);
    }
}