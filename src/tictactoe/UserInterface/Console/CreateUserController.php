<?php
declare(strict_types=1);

namespace TicTacToe\UserInterface\Console;

use TicTacToe\Entity\User;
use TicTacToe\UseCase\ResponseInterface;
use TicTacToe\UseCase\UseCaseInterface;

class CreateUserController
{
    private $useCase;
    private $response;

    public function __construct(UseCaseInterface $useCase, ResponseInterface $response)
    {
        $this->useCase = $useCase;
        $this->response = $response;
    }

    public function render(string $userName): ResponseInterface
    {
        $this->useCase->execute(['user' => new User($userName)]);
        $presenter = $this->useCase->getPresenter();

        if ($presenter->getError() != null)
            $this->formatErrorResponse($presenter->getError());
        else
            $this->formatResponse($presenter->getResponse());;

        return $this->response;
    }

    private function formatErrorResponse(string $error)
    {
        switch ($error) {
            case "DUPLICATEUSER":
                $this->response->setStatusCode(1);
                $errorMessage = "User already exists";
                break;
            default:
                $this->response->setStatusCode(99);
                $errorMessage = "Ops..something went wrong...";
                break;
        }

        $this->response->setData(["errorMessage" => $errorMessage]);
    }

    private function formatResponse()
    {
        $this->response->setData(["message" => "User created successfully"]);
    }
}