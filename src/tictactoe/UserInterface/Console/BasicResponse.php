<?php
declare(strict_types=1);

namespace TicTacToe\UserInterface\Console;


use TicTacToe\UseCase\ResponseInterface;

class BasicResponse implements ResponseInterface
{
    public const OK_STATUS_CODE = 0;
    protected $statusCode = 0;
    protected $data;

    public function getStatusCode(): int
    {
        return $this->statusCode;
    }

    public function setStatusCode(int $statusCode): void
    {
        $this->statusCode = $statusCode;
    }

    public function getData(): array
    {
        return $this->data;
    }

    public function setData(?array $data = []): void
    {
        $this->data = $data;
    }
}