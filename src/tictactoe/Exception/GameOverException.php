<?php
declare(strict_types=1);

namespace TicTacToe\Exception;


class GameOverException extends \Exception
{
    public function __construct()
    {
        parent::__construct("Game is already Over!");
    }
}