<?php
declare(strict_types=1);

namespace TicTacToe\Exception;


class PositionOccupiedException extends \Exception
{
    public function __construct($x, $y)
    {
        parent::__construct("Position {$x},{$y} already occupied.");
    }
}