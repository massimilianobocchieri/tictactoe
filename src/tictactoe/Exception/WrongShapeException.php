<?php
declare(strict_types=1);

namespace TicTacToe\Exception;


class WrongShapeException extends \Exception
{
    public function __construct()
    {
        parent::__construct("Players wrong Shape");
    }

}