<?php
declare(strict_types=1);

namespace TicTacToe\Exception;


class PlayersAlreadyRegisteredException extends \Exception
{
    public function __construct()
    {
        parent::__construct("Players already registered");
    }
}