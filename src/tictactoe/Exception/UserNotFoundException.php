<?php
declare(strict_types=1);

namespace TicTacToe\Exception;


class UserNotFoundException extends \Exception
{

    public function __construct(string $username)
    {
        parent::__construct("User with username:{$username} not found");
    }
}