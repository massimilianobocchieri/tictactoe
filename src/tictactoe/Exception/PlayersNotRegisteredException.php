<?php
declare(strict_types=1);

namespace TicTacToe\Exception;


class PlayersNotRegisteredException extends \Exception
{
    public function __construct()
    {
        parent::__construct("Players not regestered");
    }
}