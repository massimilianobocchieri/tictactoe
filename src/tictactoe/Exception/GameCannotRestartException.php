<?php
declare(strict_types=1);

namespace TicTacToe\Exception;


class GameCannotRestartException extends \Exception
{
    public function __construct()
    {
        parent::__construct("Game already started or finished");
    }
}