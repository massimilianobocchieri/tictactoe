<?php
declare(strict_types=1);

namespace TicTacToe\ValueObject;


use MyCLabs\Enum\Enum;

class GameCoordinates extends Enum
{
    const A1 = [0, 0];
    const A2 = [0, 1];
    const A3 = [0, 2];
    const B1 = [1, 0];
    const B2 = [1, 1];
    const B3 = [1, 2];
    const C1 = [2, 0];
    const C2 = [2, 1];
    const C3 = [2, 2];

    public function getCoordinate(): Coordinate
    {
        return new Coordinate($this->getValue()[0], $this->getValue()[1]);
    }
}