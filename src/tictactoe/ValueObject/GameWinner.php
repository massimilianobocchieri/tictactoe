<?php
declare(strict_types=1);

namespace TicTacToe\ValueObject;

use MyCLabs\Enum\Enum;

class GameWinner extends Enum
{
    const CROSS = 'CROSS';
    const CIRCLE = 'CIRCLE';
    const TIE = 'TIE';
    const NULL = null;
}