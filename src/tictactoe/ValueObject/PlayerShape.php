<?php
declare(strict_types=1);

namespace TicTacToe\ValueObject;

use MyCLabs\Enum\Enum;

class PlayerShape extends Enum
{
    const CIRCLE = 'CIRCLE';
    const CROSS = 'CROSS';
    const EMPTY = '';
}