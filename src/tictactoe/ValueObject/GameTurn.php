<?php
declare(strict_types=1);

namespace TicTacToe\ValueObject;

use MyCLabs\Enum\Enum;

class GameTurn extends Enum
{
    const CROSS = 'CROSS';
    const CIRCLE = 'CIRCLE';
    const NULL = null;
}