<?php
declare(strict_types=1);

namespace TicTacToe\ValueObject;


class CrossShape extends GameShape
{
    public function __toString()
    {
        return "X";
    }
}