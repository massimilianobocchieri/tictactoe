<?php
declare(strict_types=1);

namespace TicTacToe\ValueObject;

abstract class GameShape
{
    private $coordinate;

    public function __construct(?Coordinate $coordinate = null)
    {
        $this->coordinate = $coordinate;
    }

    public function getCoordinate(): ?Coordinate
    {
        return $this->coordinate;
    }
}