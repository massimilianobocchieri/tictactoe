<?php
declare(strict_types=1);

namespace TicTacToe\ValueObject;

use MyCLabs\Enum\Enum;

class GameStatus extends Enum
{
    const STARTED = 'STARTED';
    const FINISHED = 'FINISHED';
}