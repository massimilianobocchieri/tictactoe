<?php
declare(strict_types=1);

namespace TicTacToe\ValueObject;


class EmptyShape extends GameShape
{
    public function __toString()
    {
        return " ";
    }
}