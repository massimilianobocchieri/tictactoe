<?php
declare(strict_types=1);

namespace TicTacToe\ValueObject;


class CircleShape extends GameShape
{
    public function __toString()
    {
        return "O";
    }

}