<?php
declare(strict_types=1);

namespace TicTacToe\ValueObject;

use MyCLabs\Enum\Enum;

class GameMatrixDirections extends Enum
{
    const VERTICAL = 'VERTICAL';
    const HORIZONTAL = 'HORIZONTAL';
    const DIAGONAL = 'DIAGONAL';
}