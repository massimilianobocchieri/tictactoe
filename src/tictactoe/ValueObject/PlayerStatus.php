<?php
declare(strict_types=1);

namespace TicTacToe\ValueObject;

use MyCLabs\Enum\Enum;

class PlayerStatus extends Enum
{
    const WINNER = 'WINNER';
    const LOSER = 'LOSER';
    const TIE = 'TIE';
    const PLAYING = 'PLAYING';
}