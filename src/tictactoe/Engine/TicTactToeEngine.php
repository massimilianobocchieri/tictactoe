<?php
declare(strict_types=1);

namespace TicTacToe\Engine;

use TicTacToe\Entity\{
    Game, GamePlayer
};
use TicTacToe\Exception\GameOverException;
use TicTacToe\Exception\PlayersNotRegisteredException;
use TicTacToe\Exception\PositionOccupiedException;
use TicTacToe\Exception\WaitYourTurnException;
use TicTacToe\Exception\WrongShapeException;
use TicTacToe\ValueObject\{
    GameCoordinates
};

interface TicTactToeEngine
{
    /**
     * @throws WrongShapeException
     */
    public function registerPlayers(GamePlayer $circlePlayer, GamePlayer $crossPlayer): void;

    /**
     * @throws GameCannotRestartException
     * @throws PlayersNotRegisteredException
     */
    public function startGame(): void;

    /**
     * @throws PlayersNotRegisteredException
     * @throws PositionOccupiedException
     * @throws WaitYourTurnException
     * @throws GameOverException
     */
    public function playCircle(GameCoordinates $coordinate): void;


    /**
     * @throws PlayersNotRegisteredException
     * @throws PositionOccupiedException
     * @throws WaitYourTurnException
     * @throws GameOverException
     */
    public function playCross(GameCoordinates $coordinate): void;

    public function checkWinner(): ?GamePlayer;

    public function isTie(): bool;

    public function getGameMatrix(): GameMatrix;

    public function getGame(): Game;

    public function getCirclePlayer(): GamePlayer;

    public function getCrossPlayer(): GamePlayer;
}