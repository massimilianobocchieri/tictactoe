<?php
declare(strict_types=1);

namespace TicTacToe\Engine;


use TicTacToe\Entity\{
    Game, GamePlayer
};
use TicTacToe\Exception\{
    GameCannotRestartException,
    GameOverException,
    PlayersAlreadyRegisteredException,
    PlayersNotRegisteredException,
    PositionOccupiedException,
    WaitYourTurnException,
    WrongShapeException
};
use TicTacToe\ValueObject\{
    CircleShape, CrossShape, EmptyShape, GameCoordinates, GameShape, GameStatus, GameTurn, GameWinner, PlayerShape
};

class TicTactToeEngineImpl implements TicTactToeEngine
{
    protected $game;

    public function __construct(Game $game)
    {
        $this->game = $game;
        $this->game->setGameMatrix(new GameMatrix());
    }

    /**
     * @throws WrongShapeException
     * @throws PlayersAlreadyRegisteredException
     */
    public function registerPlayers(GamePlayer $circlePlayer, GamePlayer $crossPlayer): void
    {
        if ($this->playersRegistered())
            throw new PlayersAlreadyRegisteredException();

        if (!$this->checkPlayersShape($circlePlayer->getShape(), $crossPlayer->getShape()))
            throw new WrongShapeException();

        $this->game->setCirclePlayer($circlePlayer);
        $this->game->setCrossPlayer($crossPlayer);
    }

    private function playersRegistered(): bool
    {
        return $this->game->getCirclePlayer() !== null && $this->game->getCrossPlayer() != null;
    }

    private function checkPlayersShape(GameShape $circleShape, GameShape $crossShape): bool
    {
        return ($circleShape instanceof CircleShape && $crossShape instanceof CrossShape);
    }

    /**
     * @throws GameCannotRestartException
     * @throws PlayersNotRegisteredException
     */
    public function startGame(): void
    {
        if (!$this->playersRegistered())
            throw new PlayersNotRegisteredException();

        if ($this->game->getStatus() != null)
            throw new GameCannotRestartException();

        $this->game->setStatus(new GameStatus(GameStatus::STARTED));
        $this->game->setCurrentTurn(new GameTurn(GameTurn::NULL));
        $this->game->setWinner(new GameWinner(GameWinner::NULL));
    }

    /**
     * @throws PlayersNotRegisteredException
     * @throws PositionOccupiedException
     * @throws WaitYourTurnException
     * @throws GameOverException
     */
    public function playCircle(GameCoordinates $coordinate): void
    {
        $this->play(new PlayerShape(PlayerShape::CIRCLE), $coordinate);
    }

    /**
     * @throws PlayersNotRegisteredException
     * @throws PositionOccupiedException
     * @throws WaitYourTurnException
     * @throws GameOverException
     */
    private function play(PlayerShape $shape, GameCoordinates $coordinate): void
    {
        if (!$this->playersRegistered())
            throw new PlayersNotRegisteredException();

        if ($this->game->getStatus()->equals(new GameStatus(GameStatus::FINISHED)))
            throw new GameOverException();

        if (!$this->checkTurn($shape))
            throw new WaitYourTurnException();

        $this->initGameTurnIfFirstPlay($shape);

        $this->game->getGameMatrix()->writeElement(
            ($shape == PlayerShape::CROSS) ?
                new CrossShape($coordinate->getCoordinate())
                : new CircleShape($coordinate->getCoordinate()));

        $this->updateGameStatus();

        $this->gameTurnSwitch($shape); //turn if game is in progress...
    }

    private function checkTurn(PlayerShape $shape): bool
    {
        $firstPlay = $this->game->getCurrentTurn()->equals(new GameTurn(GameTurn::NULL));
        $validTurn = $this->game->getCurrentTurn()->getValue() == $shape->getValue();

        return ($firstPlay || $validTurn);
    }

    private function initGameTurnIfFirstPlay(PlayerShape $shape): void
    {
        if ($this->game->getCurrentTurn()->equals(new GameTurn(GameTurn::NULL)))
            $this->game->setCurrentTurn(new GameTurn($shape->getValue()));
    }

    private function updateGameStatus(): void
    {

        $winnerShape = $this->verifyWinner();

        if ($winnerShape instanceof CrossShape) {
            $this->game->setStatus(new GameStatus(GameStatus::FINISHED));
            $this->game->setWinner(new GameWinner(GameWinner::CROSS));
        } else if ($winnerShape instanceof CircleShape) {
            $this->game->setStatus(new GameStatus(GameStatus::FINISHED));
            $this->game->setWinner(new GameWinner(GameWinner::CIRCLE));
        } else if ($this->getGameMatrix()->isFull()) {
            $this->game->setStatus(new GameStatus(GameStatus::FINISHED));
            $this->game->setWinner(new GameWinner(GameWinner::TIE));
        }
    }

    private function verifyWinner(): GameShape
    {
        $rowWinnerShape = $this->getGameMatrix()->checkRows();

        if (!($rowWinnerShape instanceof EmptyShape))
            return $rowWinnerShape;

        $colsWinnerShape = $this->getGameMatrix()->checkCols();

        if (!($colsWinnerShape instanceof EmptyShape))
            return $colsWinnerShape;

        $diagonalWinnerShape = $this->getGameMatrix()->checkDiagonals();

        if (!($diagonalWinnerShape instanceof EmptyShape))
            return $diagonalWinnerShape;

        return new EmptyShape();
    }

    public function getGameMatrix(): GameMatrix
    {
        return $this->game->getGameMatrix();
    }

    private function gameTurnSwitch(PlayerShape $shape): void
    {
        if ($this->game->getStatus() == new GameStatus(GameStatus::FINISHED))
            return;

        $this->game->setCurrentTurn(($shape->equals(new PlayerShape(PlayerShape::CIRCLE)))
            ? new GameTurn(GameTurn::CROSS)
            : new GameTurn(GameTurn::CIRCLE));
    }

    /**
     * @throws PlayersNotRegisteredException
     * @throws PositionOccupiedException
     * @throws WaitYourTurnException
     * @throws GameOverException
     */
    public function playCross(GameCoordinates $coordinate): void
    {
        $this->play(new PlayerShape(PlayerShape::CROSS), $coordinate);
    }

    public function checkWinner(): ?GamePlayer
    {
        if ($this->game->getStatus() != new GameStatus(GameStatus::FINISHED))
            return null;

        $winner = $this->game->getWinner();

        if ($winner == new GameWinner(GameWinner::CROSS))
            return $this->game->getCrossPlayer();
        elseif ($winner == new GameWinner(GameWinner::CIRCLE))
            return $this->game->getCirclePlayer();
        else
            return null;
    }

    public function isTie(): bool
    {
        return ($this->game->getStatus()->equals(new GameStatus(GameStatus::FINISHED)) &&
            $this->game->getWinner()->equals(new GameWinner(GameWinner::TIE)));
    }

    public function getGame(): Game
    {
        return $this->game;
    }

    public function getCirclePlayer(): GamePlayer
    {
        return $this->game->getCirclePlayer();
    }

    public function getCrossPlayer(): GamePlayer
    {
        return $this->game->getCrossPlayer();
    }
}