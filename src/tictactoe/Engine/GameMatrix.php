<?php
declare(strict_types=1);

namespace TicTacToe\Engine;


use TicTacToe\Exception\PositionOccupiedException;
use TicTacToe\ValueObject\{
    CircleShape, CrossShape, EmptyShape, GameMatrixDirections, GameShape
};

class GameMatrix
{
    private const MATRIX_DIMENSION = 3;
    protected $arrayMatrix;

    public function __construct()
    {
        $this->initArrayMatrix(self::MATRIX_DIMENSION);
    }

    private function initArrayMatrix(int $dimension): void
    {
        $this->arrayMatrix = [];
        for ($row = 0; $row < $dimension; $row++)
            for ($col = 0; $col < $dimension; $col++)
                $this->arrayMatrix[$row][$col] = new EmptyShape();

    }

    /**
     * @throws PositionOccupiedException
     */
    public function writeElement(GameShape $element): void
    {
        $x = $element->getCoordinate()->getX();
        $y = $element->getCoordinate()->getY();

        //check Range

        if (!$this->arrayMatrix[$x][$y] instanceof EmptyShape)
            throw new PositionOccupiedException($x, $y);

        $this->arrayMatrix[$x][$y] = $element;

    }

    public function checkRows(): GameShape
    {
        return $this->linesChecker(new GameMatrixDirections(GameMatrixDirections::HORIZONTAL));
    }

    private function linesChecker(GameMatrixDirections $direction): GameShape
    {
        $crossCounter = 0;
        $circleCounter = 0;

        for ($i = 0; $i < count($this->arrayMatrix); $i++) {
            for ($j = 0; $j < count($this->arrayMatrix[$i]); $j++) {
                if ($direction->equals(new GameMatrixDirections(GameMatrixDirections::VERTICAL)))
                    $shape = $this->arrayMatrix[$j][$i];
                else if ($direction->equals(new GameMatrixDirections(GameMatrixDirections::HORIZONTAL)))
                    $shape = $this->arrayMatrix[$i][$j];
                else {
                    $shape = ($i === 0) ? $this->arrayMatrix[$j][$j] : $this->arrayMatrix[$j][(self::MATRIX_DIMENSION - 1) - $j];

                }
                if ($shape instanceof CircleShape)
                    $circleCounter++;
                else if ($shape instanceof CrossShape)
                    $crossCounter++;

            }

            if ($circleCounter == 3)
                return new CircleShape();

            if ($crossCounter == 3)
                return new CrossShape();

            $crossCounter = 0;
            $circleCounter = 0;

            if ($direction->equals(new GameMatrixDirections(GameMatrixDirections::DIAGONAL)) && $i > 0)
                break;
        }

        return new EmptyShape();
    }

    public function checkCols(): GameShape
    {
        return $this->linesChecker(new GameMatrixDirections(GameMatrixDirections::VERTICAL));
    }

    public function checkDiagonals(): GameShape
    {
        return $this->linesChecker(new GameMatrixDirections(GameMatrixDirections::DIAGONAL));
    }

    public function isFull(): bool
    {
        for ($row = 0; $row < count($this->arrayMatrix); $row++)
            for ($col = 0; $col < count($this->arrayMatrix[$row]); $col++)
                if ($this->arrayMatrix[$row][$col] instanceof EmptyShape)
                    return false;

        return true;
    }

    public function __toString()
    {
        $separator = "|";
        $result = "";
        $lastRow = self::MATRIX_DIMENSION - 1;
        $lastColumn = self::MATRIX_DIMENSION - 1;

        for ($row = 0; $row < self::MATRIX_DIMENSION; $row++) {
            for ($col = 0; $col < self::MATRIX_DIMENSION; $col++) {
                $result .= $this->arrayMatrix[$row][$col] . (($col < $lastColumn) ? $separator : "");
            }

            $result .= PHP_EOL;
            if ($row < $lastRow)
                $result .= "-----" . PHP_EOL;
        }
        return $result;
    }
}
