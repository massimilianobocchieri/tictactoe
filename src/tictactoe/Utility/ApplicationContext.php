<?php
declare(strict_types=1);

namespace TicTacToe\Utility;


use TicTacToe\Engine\TicTactToeEngineImpl;
use TicTacToe\Entity\Game;
use TicTacToe\Gateway\Double\InMemoryUserGateway;
use TicTacToe\TestSampleDataUtils;

class ApplicationContext
{
    public static $userGateway;
    public static $gameEngine;


    public static function initContext(): void
    {
        self::initUserGateway();
        self::initGameEngine();
    }

    private static function initUserGateway(): void
    {
        if (self::$userGateway == null)
            self::$userGateway = new InMemoryUserGateway(TestSampleDataUtils::makeSampleUsers());
    }

    private static function initGameEngine(): void
    {
        if (self::$gameEngine == null)
            self::createGameEngine();
    }

    public static function createGameEngine(): void
    {
        self::$gameEngine = new TicTactToeEngineImpl(new Game());
    }
}