<?php
declare(strict_types=1);

namespace TicTacToe\Utility;


use TicTacToe\ValueObject\GameCoordinates;
use TicTacToe\ValueObject\PlayerShape;

trait GameSymbolsTranslator
{
    public static function translateShape($shape): PlayerShape
    {
        return ($shape == "X") ? new PlayerShape(PlayerShape::CROSS) : new PlayerShape(PlayerShape::CIRCLE);
    }

    public static function translateCoordinate($coordinate): GameCoordinates
    {
        $coordinateSymbol = null;
        switch ($coordinate) {
            case "A1":
                $coordinateSymbol = GameCoordinates::A1;
                break;
            case "A2":
                $coordinateSymbol = GameCoordinates::A2;
                break;
            case "A3":
                $coordinateSymbol = GameCoordinates::A3;
                break;
            case "B1":
                $coordinateSymbol = GameCoordinates::B1;
                break;
            case "B2":
                $coordinateSymbol = GameCoordinates::B2;
                break;
            case "B3":
                $coordinateSymbol = GameCoordinates::B3;
                break;
            case "C1":
                $coordinateSymbol = GameCoordinates::C1;
                break;
            case "C2":
                $coordinateSymbol = GameCoordinates::C2;
                break;
            case "C3":
                $coordinateSymbol = GameCoordinates::C3;
                break;
        }

        return new GameCoordinates($coordinateSymbol);
    }
}