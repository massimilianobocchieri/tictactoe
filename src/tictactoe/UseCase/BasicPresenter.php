<?php
declare(strict_types=1);

namespace TicTacToe\UseCase;


class BasicPresenter implements Presenter
{
    protected $error;
    protected $response;

    public function presentResponse(array $response = []): void
    {
        $this->response = $response;
    }

    public function presentError(string $error): void
    {
        $this->error = $error;
    }

    public function getResponse()
    {
        return $this->response;
    }

    public function getError()
    {
        return $this->error;
    }
}