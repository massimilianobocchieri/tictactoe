<?php
declare(strict_types=1);

namespace TicTacToe\UseCase;


use TicTacToe\Gateway\UserGateway;

class ListUsersUseCase implements UseCaseInterface
{
    protected $gateway;
    protected $presenter;

    public function __construct(UserGateway $gateway, Presenter $presenter)
    {
        $this->gateway = $gateway;
        $this->presenter = $presenter;
    }


    public function execute(array $request = []): void
    {
        $this->presenter->presentResponse(["data" => $this->gateway->findAll()]);
    }

    public function getPresenter(): Presenter
    {
        return $this->presenter;
    }


}