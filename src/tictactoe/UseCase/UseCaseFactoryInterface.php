<?php
declare(strict_types=1);

namespace TicTacToe\UseCase;


interface UseCaseFactoryInterface
{
    public function makeUseCase(?array $request = []): UseCaseInterface;
}