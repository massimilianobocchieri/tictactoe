<?php
declare(strict_types=1);

namespace TicTacToe\UseCase;


use TicTacToe\Exception\UserNotFoundException;
use TicTacToe\Gateway\UserGateway;

class DeleteUserUseCase implements UseCaseInterface
{
    protected $gateway;
    protected $presenter;

    public function __construct(UserGateway $gateway, Presenter $presenter)
    {
        $this->gateway = $gateway;
        $this->presenter = $presenter;
    }


    public function execute(array $request = []): void
    {
        try {
            $this->gateway->delete($request["user"]);
        } catch (UserNotFoundException $e) {
            $isError = true;
            $this->presenter->presentError("USERNOTFOUND");
        } catch (\Exception $e) {
            $isError = true;
            $this->presenter->presentError("DELETEUSERERROR");
        }
    }

    public function getPresenter(): Presenter
    {
        return $this->presenter;
    }


}