<?php
declare(strict_types=1);

namespace TicTacToe\UseCase;


use TicTacToe\Engine\TicTactToeEngine;
use TicTacToe\Entity\{
    GamePlayer
};
use TicTacToe\Gateway\UserGateway;
use TicTacToe\ValueObject\CircleShape;
use TicTacToe\ValueObject\CrossShape;

class StartNewGameUseCase implements UseCaseInterface
{
    protected $gateway;
    protected $presenter;

    protected $gameEngine;

    public function __construct(UserGateway $gateway, Presenter $presenter, TicTactToeEngine $gameEngine)
    {
        $this->gateway = $gateway;
        $this->presenter = $presenter;
        $this->gameEngine = $gameEngine;
    }

    public function execute(array $request = []): void
    {
        $playerOneUserName = $request['playerOne'];
        $playerTwoUserName = $request['playerTwo'];

        $playerOneUser = $this->gateway->findByUserName($playerOneUserName);
        $playerTwoUser = $this->gateway->findByUserName($playerTwoUserName);

        if ($playerOneUser === null || $playerTwoUser == null) {
            $this->presenter->presentError("USERNOTFOUND");
            return;
        }

        //No Excpetion check coz sure about the correct Game mimic invocation
        $this->gameEngine->registerPlayers(new GamePlayer(new CircleShape(), $playerOneUser),
            new GamePlayer(new CrossShape(), $playerTwoUser));

        $this->gameEngine->startGame();

        $this->presenter->presentResponse(["crossUser" =>
            ["name" => $playerOneUser->getUserName(), "shape" => "O"],
            "circleUser" => ["name" => $playerTwoUser->getUserName(), "shape" => "X"]
        ]);
    }

    public function getPresenter(): Presenter
    {
        return $this->presenter;
    }
}