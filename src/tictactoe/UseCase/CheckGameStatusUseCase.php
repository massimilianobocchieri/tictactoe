<?php
declare(strict_types=1);

namespace TicTacToe\UseCase;


use TicTacToe\Engine\TicTactToeEngine;

class CheckGameStatusUseCase implements UseCaseInterface
{
    protected $presenter;

    protected $gameEngine;

    public function __construct(Presenter $presenter, TicTactToeEngine $gameEngine)
    {
        $this->presenter = $presenter;
        $this->gameEngine = $gameEngine;
    }


    public function execute(array $request = []): void
    {
        $isTie = $this->gameEngine->isTie();
        $winner = $this->gameEngine->checkWinner();
        $isGameOver = $winner || $isTie;
        $gameStatus = $isGameOver ? "FINISHED" : "INPROGRESS";

        $responseToPresent = ["status" => $gameStatus,
            "winner" => $isGameOver ? ($isTie ? "" : $winner->getUser()->getUserName()) : "",
            "isTie" => $isTie
        ];

        $this->presenter->presentResponse($responseToPresent);
    }

    public function getPresenter(): Presenter
    {
        return $this->presenter;
    }


}