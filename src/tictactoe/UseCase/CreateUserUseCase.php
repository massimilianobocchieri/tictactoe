<?php
declare(strict_types=1);

namespace TicTacToe\UseCase;


use TicTacToe\Exception\DuplicateUserException;
use TicTacToe\Gateway\UserGateway;

class CreateUserUseCase implements UseCaseInterface
{
    protected $gateway;
    protected $presenter;

    public function __construct(UserGateway $gateway, Presenter $presenter)
    {
        $this->gateway = $gateway;
        $this->presenter = $presenter;
    }


    public function execute(array $request = []): void
    {
        try {
            $this->gateway->save($request["user"]);
        } catch (DuplicateUserException $e) {
            $isError = true;
            $this->presenter->presentError("DUPLICATEUSER");
        } catch (\Exception $e) {
            $isError = true;
            $this->presenter->presentError("CREATEUSERERROR");
        }
    }

    public function getPresenter(): Presenter
    {
        return $this->presenter;
    }


}