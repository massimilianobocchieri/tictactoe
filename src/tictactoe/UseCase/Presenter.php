<?php
declare(strict_types=1);

namespace TicTacToe\UseCase;


interface Presenter
{
    public function presentResponse(array $response = []): void;

    public function presentError(string $error): void;

    public function getResponse();

    public function getError();
}