<?php
declare(strict_types=1);

namespace TicTacToe\UseCase;


use TicTacToe\Engine\TicTactToeEngine;
use TicTacToe\Exception\GameOverException;
use TicTacToe\Exception\PositionOccupiedException;
use TicTacToe\Exception\WaitYourTurnException;
use TicTacToe\Utility\GameSymbolsTranslator;
use TicTacToe\ValueObject\PlayerShape;

class PlayGameUseCase implements UseCaseInterface
{
    use GameSymbolsTranslator;

    protected $presenter;

    protected $gameEngine;

    public function __construct(Presenter $presenter, TicTactToeEngine $gameEngine)
    {
        $this->presenter = $presenter;
        $this->gameEngine = $gameEngine;
    }


    public function execute(array $request = []): void
    {
        $shape = GameSymbolsTranslator::translateShape($request['shape']);
        $coordinate = GameSymbolsTranslator::translateCoordinate($request['coordinate']);

        try {
            if ($shape->equals(new PlayerShape(PlayerShape::CIRCLE)))
                $this->gameEngine->playCircle($coordinate);
            else //Assume UseCase receive correct input from controllers, no defensive programming coz not API, no other else than CROSS
                $this->gameEngine->playCross($coordinate);
        } catch (GameOverException $e) {
            $this->presenter->presentError("GAMEALREADYOVER");
        } catch (WaitYourTurnException $e) {
            $this->presenter->presentError("WAITYOURTURN");
        } catch (PositionOccupiedException $e) {
            $this->presenter->presentError("POSITIONOCCUPIED");
        }

        $this->presenter->presentResponse(["matrix" => (string)$this->gameEngine->getGameMatrix()]);
    }

    public function getPresenter(): Presenter
    {
        return $this->presenter;
    }
}