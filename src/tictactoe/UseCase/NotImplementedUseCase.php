<?php
declare(strict_types=1);

namespace TicTacToe\UseCase;


class NotImplementedUseCase implements UseCaseInterface
{
    protected $presenter;

    public function __construct(Presenter $presenter)
    {
        $this->presenter = $presenter;
    }

    public function execute(array $request = []): void
    {
        $this->presenter->presentError("NOTIMPLEMENTEDUSECASE");
    }

    public function getPresenter(): Presenter
    {
        return $this->presenter;
    }
}