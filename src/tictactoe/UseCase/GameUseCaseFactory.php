<?php
declare(strict_types=1);

namespace TicTacToe\UseCase;


use TicTacToe\Utility\ApplicationContext;

class GameUseCaseFactory implements UseCaseFactoryInterface
{
    public function makeUseCase(?array $request = []): UseCaseInterface
    {
        $inputChoice = isset($request['choice']) ? (int)$request['choice'] : null;
        switch ($inputChoice) {
            case 1:
                return new CreateUserUseCase(ApplicationContext::$userGateway,
                    new BasicPresenter());
            case 2:
                return new DeleteUserUseCase(ApplicationContext::$userGateway,
                    new BasicPresenter());
            case 3:
                return new ListUsersUseCase(ApplicationContext::$userGateway,
                    new BasicPresenter());
            case 4:
                ApplicationContext::createGameEngine();
                return new StartNewGameUseCase(ApplicationContext::$userGateway,
                    new BasicPresenter(), ApplicationContext::$gameEngine);
            case 5:
                return new PlayGameUseCase(new BasicPresenter(), ApplicationContext::$gameEngine);
            case 6:
                return new CheckGameStatusUseCase(new BasicPresenter(), ApplicationContext::$gameEngine);
            default:
                return new NotImplementedUseCase(new BasicPresenter());
        }
    }
}