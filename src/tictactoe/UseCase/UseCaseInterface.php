<?php
declare(strict_types=1);

namespace TicTacToe\UseCase;


interface UseCaseInterface
{
    public function execute(array $request = []): void;

    public function getPresenter(): Presenter;
}