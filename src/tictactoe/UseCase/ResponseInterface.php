<?php
declare(strict_types=1);

namespace TicTacToe\UseCase;


interface ResponseInterface
{
    public function getStatusCode(): int;

    public function getData(): array;

    public function setStatusCode(int $statusCode): void;

    public function setData(?array $data = []): void;
}