<?php
declare(strict_types=1);

namespace TicTacToe\Gateway\Double;


use TicTacToe\Entity\User;
use TicTacToe\Exception\{
    DuplicateUserException, UserNotFoundException
};
use TicTacToe\Gateway\UserGateway;

class InMemoryUserGateway implements UserGateway
{
    protected $users;

    public function __construct(?array $users = [])
    {
        $this->users = $users;
    }

    /**
     * @throws DuplicateUserException
     */
    function save(User $user): User
    {
        if (isset($this->users[$user->getUserName()]))
            throw new DuplicateUserException($user->getUserName());

        $this->users[$user->getUserName()] = $user;

        return $user;
    }

    /**
     * @throws UserNotFoundException
     */
    function delete(User $user): void
    {
        if (!isset($this->users[$user->getUserName()]))
            throw new UserNotFoundException($user->getUserName());

        unset($this->users[$user->getUserName()]);
    }

    function findByUserName(string $username): ?User
    {
        return isset($this->users[$username]) ? $this->users[$username] : null;
    }

    function findAll(): array
    {
        return array_values($this->users);
    }
}