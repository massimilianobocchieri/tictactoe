<?php
declare(strict_types=1);

namespace TicTacToe\Gateway;


use TicTacToe\Entity\User;
use TicTacToe\Exception\{
    DuplicateUserException, UserNotFoundException
};

interface UserGateway
{
    /**
     * @throws DuplicateUserException
     */
    function save(User $user): User;

    /**
     * @throws UserNotFoundException
     */
    function delete(User $user): void;

    function findByUserName(string $username): ?User;

    function findAll(): array;

}