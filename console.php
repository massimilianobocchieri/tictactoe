<?php
declare(strict_types=1);

use TicTacToe\UserInterface\Console\Console;

$loader = require __DIR__ . "/vendor/autoload.php";

$app = new Console();

$app->execute();

